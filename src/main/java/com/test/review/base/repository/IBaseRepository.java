package com.test.review.base.repository;

import com.test.review.base.model.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IBaseRepository<TEntity extends BaseEntity> extends JpaRepository<TEntity, String> {
}
