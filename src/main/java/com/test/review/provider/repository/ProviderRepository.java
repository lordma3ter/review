package com.test.review.provider.repository;

import com.test.review.base.repository.IBaseRepository;
import com.test.review.provider.model.Provider;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends IBaseRepository<Provider> {
}
