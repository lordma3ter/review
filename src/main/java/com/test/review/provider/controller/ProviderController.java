package com.test.review.provider.controller;

import com.test.review.provider.model.Provider;
import com.test.review.provider.payload.AddProviderRequest;
import com.test.review.provider.payload.EditProviderRequest;
import com.test.review.provider.service.ProviderService;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/provider", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProviderController {

    private final ProviderService providerService;

    @Autowired
    public ProviderController(ProviderService providerService) {
        this.providerService = providerService;
    }

    @GetMapping
    public ResponseEntity<Page<Provider>> browse(@RequestParam(defaultValue = "0") @Range(min = 0) int page,
                                                 @RequestParam(defaultValue = "20") @Range(min = 1, max = 20) int size) throws Throwable {
        return ResponseEntity.ok(providerService.getProviders(page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Provider> read(@PathVariable("id") String id) throws Throwable {
        return ResponseEntity.ok(providerService.findById(id));
    }

    @PostMapping()
    public ResponseEntity<Provider> create(@RequestBody AddProviderRequest request) throws Throwable {
        return ResponseEntity.ok(providerService.save(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Provider> update(@PathVariable("id") String id, @RequestBody EditProviderRequest request) throws Throwable {
        return ResponseEntity.ok(providerService.update(id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) throws Throwable {
        return ResponseEntity.ok(providerService.deleteProvider(id));
    }

}
