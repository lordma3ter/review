package com.test.review.product.mapper;

import com.test.review.product.model.Comment;
import com.test.review.product.payload.CommentRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommentMapper {

    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    Comment toComment(CommentRequest request);
}
