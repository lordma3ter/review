package com.test.review.product.mapper;

import com.test.review.product.model.Product;
import com.test.review.product.payload.AddProductRequest;
import com.test.review.product.payload.EditProductRequest;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    Product toProduct(AddProductRequest request);

    Product toProduct(EditProductRequest request);

    void update(@MappingTarget Product entity, EditProductRequest updateEntity);

}
