package com.test.review.product.repository;

import com.test.review.base.repository.IBaseRepository;
import com.test.review.product.model.Comment;
import com.test.review.product.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends IBaseRepository<Product> {

    @Query("FROM Product p where p.visible = :visible")
    Page<Product> findProductsByVisible(@Param("visible") boolean visible, PageRequest page);

    @Query("FROM Product p where p.visible = :visible AND p.id = :id")
    Optional<Product> findByIdAndVisible(@Param("visible") boolean visible, @Param("id") String id);

    @Query("SELECT p.comments FROM Product p where p.visible = :visible")
    Page<Comment> findCommentsByVisible(@Param("visible") boolean visible, PageRequest page);
}
