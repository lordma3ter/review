package com.test.review.product.controller;

import com.test.review.product.model.Comment;
import com.test.review.product.model.Product;
import com.test.review.product.payload.CommentRequest;
import com.test.review.product.service.ProductService;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/um/product", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ProductUMController {

    private final ProductService productService;

    @Autowired
    public ProductUMController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<Page<Product>> browse(@RequestParam(defaultValue = "0") @Range(min = 0) int page,
                                                @RequestParam(defaultValue = "20") @Range(min = 1, max = 20) int size) throws Throwable {
        return ResponseEntity.ok(productService.getProductsByVisible(true, page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> read(@PathVariable("id") String id) throws Throwable {
        return ResponseEntity.ok(productService.findByIdAndVisible(true, id));
    }

    @GetMapping("/comments")
    public ResponseEntity<Page<Comment>> browseComment(@RequestParam(defaultValue = "0") @Range(min = 0) int page,
                                                       @RequestParam(defaultValue = "20") @Range(min = 1, max = 20) int size) throws Throwable {
        return ResponseEntity.ok(productService.getCommentsByVisible(true, page, size));
    }

    @PostMapping("/{id}/comments")
    public ResponseEntity<Comment> addComment(@PathVariable("id") String postId, @Validated @RequestBody CommentRequest request) throws Throwable {
        return ResponseEntity.ok(productService.addComment(postId, request));
    }

}
