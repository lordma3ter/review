package com.test.review.product.controller;

import com.test.review.product.model.Product;
import com.test.review.product.payload.AddProductRequest;
import com.test.review.product.payload.EditProductRequest;
import com.test.review.product.service.ProductService;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sm/product", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ProductSMController {

    private final ProductService productService;

    @Autowired
    public ProductSMController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<Page<Product>> browse(@RequestParam(defaultValue = "0") @Range(min = 0) int page,
                                                @RequestParam(defaultValue = "20") @Range(min = 1, max = 20) int size) throws Throwable {
        return ResponseEntity.ok(productService.getProducts(page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> read(@PathVariable("id") String id) throws Throwable {
        return ResponseEntity.ok(productService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Product> create(@Validated @RequestBody AddProductRequest request) throws Throwable {
        return ResponseEntity.ok(productService.saveProduct(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable("id") String id, @RequestBody EditProductRequest request) throws Throwable {
        return ResponseEntity.ok(productService.updateProduct(id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) throws Throwable {
        return ResponseEntity.ok(productService.deleteProduct(id));
    }

}
