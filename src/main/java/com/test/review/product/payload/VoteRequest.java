package com.test.review.product.payload;

import com.test.review.product.constant.VoteOption;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class VoteRequest implements Serializable {

    @NotNull
    private VoteOption vote;
}
