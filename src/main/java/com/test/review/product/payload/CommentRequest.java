package com.test.review.product.payload;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

@Data
public class CommentRequest implements Serializable {

    @NotEmpty
    private String text;
}
