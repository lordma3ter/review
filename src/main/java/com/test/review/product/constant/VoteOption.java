package com.test.review.product.constant;

import lombok.Getter;

@Getter
public enum VoteOption {
    STAR_1,
    STAR_2,
    STAR_3,
    STAR_4,
}
