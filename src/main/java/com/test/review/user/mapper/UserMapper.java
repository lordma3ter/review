package com.test.review.user.mapper;

import com.test.review.user.model.User;
import com.test.review.user.payload.AddUserRequest;
import com.test.review.user.payload.EditUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toUser(AddUserRequest request);

    User toUser(EditUserRequest request);

    void update(@MappingTarget User entity, EditUserRequest updateEntity);

}
