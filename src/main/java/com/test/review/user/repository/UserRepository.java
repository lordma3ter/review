package com.test.review.user.repository;

import com.test.review.base.repository.IBaseRepository;
import com.test.review.user.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends IBaseRepository<User> {
}
